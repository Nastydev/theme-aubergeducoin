<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header();
/**
if (have_posts()) {
    while (have_posts()) {
        echo '<div class="inside-article">';
        the_post();?>
        <h2><a href="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
       <?php if (the_post_thumbnail_url() == true ){?>
        <img src="<?php the_post_thumbnail_url(); ?>" alt="">
        <?php}
        the_excerpt();
        echo '</div>';
    }
} */

if(have_posts()){
    while(have_posts()){
        the_post();
        the_content();
    }
}

get_footer();
